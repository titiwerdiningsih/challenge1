import java.util.Scanner;


public class Challenge1 {
    public static Scanner in = new Scanner(System.in);
    public static void main(String[] args){
        menuUtama();
    }
    private static boolean mainLoop = true;
    private static boolean subLoop1 = true;
    private static boolean subLoop2 = true;
    private static final Double[] numberStorage = {0.0, 0.0, 0.0};
    private static Double result = 0.0;

    private static String getInput(){
        Scanner sc = new Scanner(System.in);
        return sc.nextLine();
    }
    private static void menuUtama(){
        while(mainLoop){
            System.out.println("------------------------------------");
            System.out.println("Kalkulator Penghitung Luas dan Kolom");
            System.out.println("------------------------------------");
            System.out.println("Menu");
            System.out.println("1. Hitung Luas Bidang");
            System.out.println("2. Hitung Volume");
            System.out.println("0. Tutup Aplikasi");
            System.out.println("------------------------------------");
            System.out.print("PILIHAN MENU: ");
            try{
                int checkMainMenu = Integer.parseInt(getInput());
                if(checkMainMenu > 2){
                    System.out.println("Masukan menu salah, tidak ada dalam daftar");
                }
                else{
                    subLoop1 = true;
                    switch(checkMainMenu){
                        case 0: mainLoop = false; break;
                        case 1: menuLuas(); break;
                        case 2: menuVolum(); break;
                    }
                }
            }
            catch(Exception e){
                System.out.println("Masukan salah, masukan harus berupa angka!");
            }
        }
    }

    private static void menuLuas(){
        while(subLoop1){
            System.out.println("------------------------------------");
            System.out.println("Pilih Bidang yang akan dihitung");
            System.out.println("------------------------------------");
            System.out.println("Menu");
            System.out.println("1. Persegi");
            System.out.println("2. Lingkaran");
            System.out.println("3. Segitiga");
            System.out.println("4. Persegi Panjang");
            System.out.println("0. Kembali ke menu sebelumnya");
            System.out.println("------------------------------------");
            System.out.print("PILIHAN MENU: ");
            try{
                int checkMainMenu = Integer.parseInt(getInput());
                if(checkMainMenu > 4){
                    System.out.println("Masukan menu salah, tidak ada dalam daftar");
                }
                else{
                    subLoop2 = true;

                    switch(checkMainMenu){
                        case 0: subLoop1 = false; break;
                        case 1: hitungPersegi(); break;
                        case 2: hitungLingkaran(); break;
                        case 3: hitungSegitiga(); break;
                        case 4: hitungPersegiPanjang(); break;
                    }
                }
            }
            catch(Exception e){
                System.out.println("Masukan salah, masukan harus berupa angka!");
            }
        }
    }

    private static void menuVolum(){
        while(subLoop1){
            System.out.println("------------------------------------");
            System.out.println("Pilih Bidang yang akan dihitung");
            System.out.println("------------------------------------");
            System.out.println("Menu");
            System.out.println("1. Kubus");
            System.out.println("2. Balok");
            System.out.println("3. Tabung");
            System.out.println("0 Kembali ke menu sebelumnya");
            System.out.println("------------------------------------");
            System.out.print("PILIHAN MENU: ");
            try{
                int checkMainMenu = Integer.parseInt(getInput());
                if(checkMainMenu > 3){
                    System.out.println("Masukan menu salah, tidak ada dalam daftar");
                }
                else{
                    subLoop2 = true;

                    switch(checkMainMenu){
                        case 0: subLoop1 = false; break;
                        case 1: hitungKubus(); break;
                        case 2: hitungBalok(); break;
                        case 3: hitungTabung(); break;
                    }
                }
            }
            catch(Exception e){
                System.out.println("Masukan salah, masukan harus berupa angka!");
            }
        }
    }

    private static void hitungPersegi(){
        while(subLoop2){
            System.out.println("------------------------------------");
            System.out.println("Hitung Persegi");
            System.out.println("------------------------------------");
            try{
                System.out.print("Masukkkan sisi persegi: ");
                numberStorage[0] = Double.parseDouble(getInput());
            }
            catch(Exception e){
                System.out.println("Masukan salah, masukan harus berupa angka!");
            }
            result = numberStorage[0] * numberStorage[0];
            System.out.println("\nProcessing...");
            System.out.println("\nLuas Persegi: " + result);
            System.out.println("------------------------------------");
            System.out.print("tekan apa saja untuk kembali ke manu utama");
            in.next();
            menuUtama();
            subLoop2 = false;
            subLoop1 = false;
        }
    }

    private static void hitungLingkaran(){
        while(subLoop2){
            System.out.println("------------------------------------");
            System.out.println("Hitung Lingkaran");
            System.out.println("------------------------------------");
            try{
                System.out.print("Masukkkan jari-jari lingkaran: ");
                numberStorage[0] = Double.parseDouble(getInput());
            }
            catch(Exception e){
                System.out.println("Masukan salah, masukan harus berupa angka!");
            }
            result = 3.14 * numberStorage[0] * numberStorage[0];
            System.out.println("\nProcessing...");
            System.out.println("\nLuas Lingkaran: " + result);
            System.out.println("------------------------------------");
            System.out.print("tekan apa saja untuk kembali ke manu utama");
            in.next();
            menuUtama();
            subLoop2 = false;
            subLoop1 = false;
        }
    }

    private static void hitungPersegiPanjang(){
        while(subLoop2){
            System.out.println("------------------------------------");
            System.out.println("Hitung Persegi Panjang");
            System.out.println("------------------------------------");

            try{
                System.out.print("Masukkkan Panjang: ");
                numberStorage[0] = Double.parseDouble(getInput());
                try{
                    System.out.print("Masukkkan Lebar: ");
                    numberStorage[1] = Double.parseDouble(getInput());
                }
                catch(Exception e){
                    System.out.println("Masukan salah, masukan harus berupa angka!");
                }
            }
            catch(Exception e){
                System.out.println("Masukan salah, masukan harus berupa angka!");
            }
            result = numberStorage[0] * numberStorage[1];
            System.out.println("\nProcessing...");
            System.out.println("\nLuas Persegi Panjang: " + result);
            System.out.println("------------------------------------");
            System.out.print("tekan apa saja untuk kembali ke manu utama");
            in.next();
            menuUtama();
            subLoop2 = false;
            subLoop1 = false;
        }
    }

    private static void hitungSegitiga(){
        while(subLoop2){
            System.out.println("------------------------------------");
            System.out.println("Hitung Segitiga");
            System.out.println("------------------------------------");
            System.out.print("Masukkkan alas: ");
            try{
                numberStorage[0] = Double.parseDouble(getInput());
                try{
                    System.out.print("Masukkkan tinggi: ");
                    numberStorage[1] = Double.parseDouble(getInput());
                }
                catch(Exception e){
                    System.out.println("Masukan salah, masukan harus berupa angka!");
                }
            }
            catch(Exception e){
                System.out.println("Masukan salah, masukan harus berupa angka!");
            }
            result = 0.5 * numberStorage[0] * numberStorage[1];
            System.out.println("\nProcessing...");
            System.out.println("\nLuas Segitiga: " + result);
            System.out.println("------------------------------------");
            System.out.print("tekan apa saja untuk kembali ke manu utama");
            in.next();
            menuUtama();
            subLoop2 = false;
            subLoop1 = false;
        }
    }

    private static void hitungKubus(){
        while(subLoop2){
            System.out.println("------------------------------------");
            System.out.println("Hitung Kubus");
            System.out.println("------------------------------------");
            System.out.print("Masukkkan sisi Kubus: ");
            try{
                numberStorage[0] = Double.parseDouble(getInput());
            }
            catch(Exception e){
                System.out.println("Masukan salah, masukan harus berupa angka!");
            }
            result = numberStorage[0] * numberStorage[0] * numberStorage[0];
            System.out.println("\nProcessing...");
            System.out.println("\nVolum Kubus: " + result);
            System.out.println("------------------------------------");
            System.out.print("tekan apa saja untuk kembali ke manu utama");
            in.next();
            menuUtama();
            subLoop2 = false;
            subLoop1 = false;
        }
    }

    private static void hitungBalok(){
        while(subLoop2){
            System.out.println("------------------------------------");
            System.out.println("Hitung Balok");
            System.out.println("------------------------------------");
            try{
                System.out.print("Masukkkan Panjang: ");
                numberStorage[0] = Double.parseDouble(getInput());
                try{
                    System.out.print("Masukkkan Lebar: ");
                    numberStorage[1] = Double.parseDouble(getInput());

                    try{
                        System.out.print("Masukkkan Tinggi: ");
                        numberStorage[2] = Double.parseDouble(getInput());
                    }
                    catch(Exception e){
                        System.out.println("Masukan salah, masukan harus berupa angka!");
                    }
                }
                catch(Exception e){
                    System.out.println("Masukan salah, masukan harus berupa angka!");
                }
            }
            catch(Exception e){
                System.out.println("Masukan salah, masukan harus berupa angka!");
            }
            result = numberStorage[0] * numberStorage[1] * numberStorage[2];
            System.out.println("\nProcessing...");
            System.out.println("\nVolume Balok: " + result);
            System.out.println("------------------------------------");
            System.out.print("tekan apa saja untuk kembali ke manu utama");
            in.next();
            menuUtama();
            subLoop2 = false;
            subLoop1 = false;
        }
    }

    private static void hitungTabung(){
        while(subLoop2){
            System.out.println("------------------------------------");
            System.out.println("Hitung Lingkaran");
            System.out.println("------------------------------------");
            try{
                System.out.print("Masukkkan Jari - Jari: ");
                numberStorage[0] = Double.parseDouble(getInput());
                try{
                    System.out.print("Masukkkan Tinggi: ");
                    numberStorage[1] = Double.parseDouble(getInput());
                }
                catch(Exception e){
                    System.out.println("Masukan salah, masukan harus berupa angka!");
                }
            }
            catch(Exception e){
                System.out.println("Masukan salah, masukan harus berupa angka!");
            }

            result = 3.14 * numberStorage[0] * numberStorage[0] * numberStorage[1];
            System.out.println("\nProcessing...");
            System.out.println("\nVolume Tabung: " + result);
            System.out.println("------------------------------------");
            System.out.print("tekan apa saja untuk kembali ke manu utama");
            in.next();
            menuUtama();
            subLoop2 = false;
            subLoop1 = false;
        }
    }
}
